FROM ubuntu:20.04
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    python3  \
    build-essential \
    vim \
    unzip \
    python3-pip

RUN pip install tornado
RUN pip install requests
RUN pip install usps-api
RUN mkdir /tornado
ADD app.py /tornado/
EXPOSE 8080
CMD ["python3", "/tornado/app.py"]

