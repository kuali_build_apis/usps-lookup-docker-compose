# USPS-lookup-docker-compose

a training exercize for python API developers. This simple example illustrates using the Python Tornado framework to provide a simple web service that accespt HTTP POSTs to lookup addresses at the USPS.

For an example of how this would work from Kualit Build see [https://gitlab.oit.duke.edu/mccahill/usps-address-lookup]

## Getting started

- git clone to pull a copy to your VM
- install docker
- install docker-compose
- edit the file docker-compose.yml and put a valid USPS key in for the USPS_SECRET

## Running and modifying

Whenever you change the app.py file, you can create a new version of the docker container with the command
- sudo docker-compose build

To run the container with output attached to your terminal session
- sudo docker-compose up

To run the container inb the background
- sudo docker-compose up -d

To stop the container
- sudo docker-compose down







***

